#!/bin/bash
# Blocks until the screen is unlocked.


if [  "x${WAYLAND_DISPLAY}" == "x" ]; then
    # Use xsecurelock on x

    # TODO see if all apps with a mouse or keyboard grab can be
    # minimized first?
    env \
        XSECURELOCK_PASSWORD_PROMPT=cursor \
        XSECURELOCK_SAVER=saver_blank \
        XSECURELOCK_AUTH_TIMEOUT=15 \
        XSECURELOCK_BLANK_TIMEOUT=10 \
        XSECURELOCK_BLANK_DPMS_STATE="standby" \
        XSECURELOCK_WAIT_TIME_MS=100 \
        XSECURELOCK_SHOW_DATETIME=1 \
        XSECURELOCK_SHOW_HOSTNAME=1 \
        XSECURELOCK_SWITCH_USER_COMMAND="dm-tool switch-to-greeter" \
        XSECURELOCK_SAVER_STOP_ON_DPMS=1 \
        XSECURELOCK_KEY_XF86AudioPlay_COMMAND="playerctl play-pause" \
        XSECURELOCK_KEY_XF86AudioNext_COMMAND="playerctl next" \
        XSECURELOCK_KEY_XF86AudioPrev_COMMAND="playerctl previous" \
        xsecurelock

else
    #Use swaylock on wayland
    #wlopm --off \*
    swaylock -f
fi

