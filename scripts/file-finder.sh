#!/bin/bash

findcmd() {
    fd --no-ignore --type file --exclude "H3X-Software" --exclude "H3X-Documents" . "$HOME" | sed "s#^${HOME}#~#"
}

indexcmd() {
    cat "$HOME/.cache/h3x-documents.listing"
}

filename=$({ indexcmd; findcmd; } | rofi -keep-right -dmenu -i -p Search)

# Exit code will be non-zero if rofi was cancelled.

if [ $? -ne 0 ] || [ -z "$filename" ]; then
    exit $?
fi

# Undo '~' substitution.
exo-open "${filename/#\~/$HOME}"
