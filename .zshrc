HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=1000
setopt extendedglob
unsetopt beep

# Compinit

# partial completeion
zstyle ':completion:*' list-suffixes
zstyle ':completion:*' expand prefix suffix
autoload -Uz compinit
compinit

# Enable ctrl-R reverse search
bindkey -v
bindkey '^R' history-incremental-pattern-search-backward
bindkey -v '^?' backward-delete-char

# Searching
bindkey -M vicmd '?' history-incremental-pattern-search-backward
bindkey -M vicmd '/' history-incremental-pattern-search-forward

#bindkey "^[OA" up-line-or-beginning-search
#bindkey "^[OB" down-line-or-beginning-search
#bindkey -M vicmd "k" up-line-or-beginning-search
#bindkey -M vicmd "j" down-line-or-beginning-search

# Make switch to cmd mode faster
export KEYTIMEOUT=1

# Normal backspace
bindkey "^?" backward-delete-char

# Correction
setopt CORRECT
setopt CORRECT_ALL

# Aliases

alias ls='ls -bF --color'
alias ll='ls -bFlah --color'
alias ff="find . -name"
alias grep='grep --color'
alias ffmpeg='ffmpeg -hide_banner'
alias ffprobe='ffprobe -hide_banner'
alias ffplay='ffplay -hide_banner'
alias vim="nvim"

# fzf setup
eval "$(fzf --zsh)"
export FZF_DEFAULT_OPTS='--layout=reverse'
export FZF_DEFAULT_COMMAND='fd --type file --follow'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

_fzf_compgen_path() {
    fd --hidden --follow --exclude ".git" . "$1"
}

_fzf_compgen_dir() {
    fd --type d --hidden --follow --exclude ".git" . "$1"
}

# Prompt setup

parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

hex2escape() {
    hex=$1
    printf "2;%d;%d;%d" 0x${hex:1:2} 0x${hex:3:2} 0x${hex:5:2}
}

NEWLINE=$'\n'

# Truecolor values.
MAIN_COLOR=#f0544c
SECONDARY_COLOR=#b5abab
ERROR_COLOR=#ff0000
HIGHLIGHT_COLOR=#feffa5
LINK_COLOR=#129490
BLOCK_COLOR=#ffd23f
BG_COLOR=#222222
BLUE_COLOR=#574ae2

# Convert to bash escape sequences as well, for LS_COLORS
MAIN_ESCAPE=$(hex2escape ${MAIN_COLOR})
SECONDARY_ESCAPE=$(hex2escape ${SECONDARY_COLOR})
ERROR_ESCAPE=$(hex2escape ${ERROR_COLOR})
HIGHLIGHT_ESCAPE=$(hex2escape ${HIGHLIGHT_COLOR})
LINK_ESCAPE=$(hex2escape ${LINK_COLOR})
BLOCK_ESCAPE=$(hex2escape ${BLOCK_COLOR})
BG_ESCAPE=$(hex2escape ${BG_COLOR})
BLUE_ESCAPE=$(hex2escape ${BLUE_COLOR})

# Color version
setopt prompt_subst
export  PROMPT="%F{white}%B[%n@%m] %F{${MAIN_COLOR}}%10~%b%(?.. %F{${ERROR_COLOR}}[%?])%f %F{${SECONDARY_COLOR}}\$(parse_git_branch)%f ${NEWLINE}%# "


# ls colors
export LS_COLORS="di=38;${MAIN_ESCAPE};01:ex=38;${HIGHLIGHT_ESCAPE};01:ln=38;${LINK_ESCAPE};01:or=38;${ERROR_ESCAPE};01:bd=38;${BLOCK_ESCAPE};01:cd=38;${BLOCK_ESCAPE};01:tw=38;${BG_ESCAPE};48;${MAIN_ESCAPE}:ow=38;${LINK_ESCAPE};48;${MAIN_ESCAPE}"

## Enviroment variables
PATH=~/bin:~/.local/bin:~/.local/share/gem/ruby/3.0.0/bin:$PATH:./node_modules/.bin
export EDITOR=nvim

if [ -n "$NVIM" ]; then
    export MANPAGER="nvr --servername $NVIM -c 'Man!' -o -"
    export MANWIDTH=999
else
    export MANPAGER='nvim +Man!'
    export MANWIDTH=999
fi

export GEM_HOME=$(ruby -e 'puts Gem.user_dir')

# Source local config
if [ -e ~/.zshrc.local ]; then
    source ~/.zshrc.local
fi

