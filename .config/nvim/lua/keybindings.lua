-- All keybindings defined here for easy reference.
local plugin_keybinds = {}

-- Default
vim.g.mapleader = ","

-- Open help on a vim topic by pressing ',h' on it.
vim.keymap.set("n", "<leader>h", ":help <c-r><c-w><cr>")
-- Clear search highlights.
vim.keymap.set("n", "<leader><cr>", ":noh<cr>")
-- Use jk as <esc> - to type literal 'jk', type j, wait, then type k.
vim.keymap.set("i", "jk", "<esc>")

-- Navigate wildmenu with ctrl-j/ctrl-k
vim.keymap.set("c", "<c-j>", function() return vim.fn.wildmenumode() and "<c-n>" or "<c-j>" end, { expr = true })
vim.keymap.set("c", "<c-k>", function() return vim.fn.wildmenumode() and "<c-p>" or "<c-k>" end, { expr = true })

-- Move by displayed lines when wrapped
vim.keymap.set("n", "j", "gj", { silent = true })
vim.keymap.set("n", "k", "gk", { silent = true })

-- Buffer movement
vim.keymap.set("n", "<leader>j", "<cmd>bn<cr>")
vim.keymap.set("n", "<leader>k", "<cmd>bn<cr>")

-- Spellchecking
vim.keymap.set("n", "<leader>ss", "<cmd>setlocal spell!<cr>")

-- Copy and Paste from system keyboard
vim.keymap.set({ "n", "v" }, "<leader>y", "\"+y")
vim.keymap.set({ "n", "v" }, "<leader>p", "\"+p")
vim.keymap.set({ "n", "v" }, "<leader>P", "\"+P")

-- Misc Utility
vim.keymap.set("n", "<leader>K", function()
    word = vim.fn.expand("<cword>")

    -- Work around poor error reporting from str2nr()
    local number = 0
    if word ~= "0" then
        number = vim.fn.str2nr(word)
        if number == 0 then
            number = vim.fn.str2nr(word, 16)
            if number == 0 then
                return
            end
        end
    end

    vim.lsp.util.open_floating_preview({
        vim.fn.printf("Dec: %d", number),
        vim.fn.printf("Hex: 0x%X", number),
        vim.fn.printf("Bin: 0b%b", number),
    }, "markdown")
end)


-- Terminal mode functions
-- TODO figure out how I want to navigate bash vi mode and neovim normal mode.
-- vim.keymap.set("t", "<esc>", "<c-\\><c-N>") -- Use escape to go back to normal mode
-- vim.keymap.set("t", "jk", "<c-\\><c-N>") -- interferes with j and k navigation in pagers.
-- TODO translate this to lua: tnoremap <expr> <C-R> '<C-\><C-N>"'.nr2char(getchar()).'pi'
vim.keymap.set("t", "<c-w>h", "<c-\\><c-N><c-w>h")
vim.keymap.set("t", "<c-w>j", "<c-\\><c-N><c-w>j")
vim.keymap.set("t", "<c-w>k", "<c-\\><c-N><c-w>k")
vim.keymap.set("t", "<c-w>l", "<c-\\><c-N><c-w>l")

-- Inserting closing brackets/quotes/etc
-- vim.keymap.set("i", "\"<tab>", "\"\"<left>")
-- vim.keymap.set("i", "'<tab>", "''<left>")
-- vim.keymap.set("i", "(<tab>", "()<left>")
-- vim.keymap.set("i", "[<tab>", "[]<left>")
vim.keymap.set("i", "{<tab>", "{}<left>")
vim.keymap.set("i", "{<cr>", "{<cr>}<esc>O") -- Add matching tabs, then go to insert mode between the lines.

-- Telescope
local telescope_builtin = require('telescope.builtin')
vim.keymap.set("n", "<leader>ff", telescope_builtin.find_files, {})
vim.keymap.set("n", "<c-p>", telescope_builtin.find_files, {})
vim.keymap.set("n", "<leader>fg", telescope_builtin.live_grep, {})
vim.keymap.set("n", "<leader>fb", telescope_builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", telescope_builtin.help_tags, {})
-- TODO spell suggest?
local telescope_actions = require("telescope.actions")
-- TODO other telescope actions?
plugin_keybinds["telescope"] = {
    i = {
        ["<c-j>"] = telescope_actions.move_selection_next,
        ["<c-k>"] = telescope_actions.move_selection_previous,
    },
    n = {
        ["jk"] = telescope_actions.close,
    }
}

plugin_keybinds["dressing"] = {
    input = {
        i = {
            ["<c-k>"] = "HistoryPrev",
            ["<c-j>"] = "HistoryDown",
        }
    }
}

plugin_keybinds["treesitter"] = {
    incremental_selection = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
    },
    textobjects = {
        select = {
            ["aa"] = "@parameter.outer",
            ["ia"] = "@parameter.inner",
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
        },
        move_goto_next_start = {
            ["]m"] = "@function.outer",
            ["]]"] = "@class.outer",
        },
        move_goto_next_end = {
            ["]M"] = "@function.outer",
            ["]["] = "@class.outer",
        },
        move_goto_prev_start = {
            ["[m"] = "@function.outer",
            ["[]"] = "@class.outer",
        },
        move_goto_prev_end = {
            ["[M"] = "@function.outer",
            ["[["] = "@class.outer",
        },
        swap_next = {
            ["<leader>a"] = "@parameter.inner",
        },
        swap_previous = {
            ["<leader>A"] = "@parameter.inner",
        },
    },
}

-- vimdiff keybinds
vim.keymap.set({ 'n', 'v' }, '<leader>dp', '<cmd>diffput<cr>')
vim.keymap.set({ 'n', 'v' }, '<leader>dg', '<cmd>diffget<cr>')

-- LSP / coding actions. Try to have these start with <leader>c unless they are very common.
vim.keymap.set('n', '<leader>ce', vim.diagnostic.open_float)
vim.keymap.set('n', '<leader>ch', vim.diagnostic.open_float)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next)
vim.keymap.set('n', '<leader>cq', vim.diagnostic.setloclist)

-- Use LspAttach to only map these keys after the language server attaches.
plugin_keybinds["lsp_callback"] = function(bufnr, client)
    -- Enable completion triggered by <c-x><c-o>
    vim.bo[bufnr].omnifunc = 'v:lua.vim.lsp.omnifunc'

    -- Buffer local mappings
    local opts = { buffer = bufnr }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts) -- TODO fix hover conflicting with auto-showing diagnostics
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', '<c-k>', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<space>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts)
    vim.keymap.set('n', '<leader>cD', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<leader>cr', vim.lsp.buf.rename, opts)
    vim.keymap.set({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<leader>cf', function()
        vim.lsp.buf.format { async = true }
    end, opts)

    if client.name == "rust_analyzer" then
        vim.keymap.set('n', '<leader>rr', function()
            require("ferris.methods.reload_workspace")()
        end, opts)

        vim.keymap.set('n', '<leader>re', function()
            require("ferris.methods.expand_macro")()
        end, opts)
    end
end

-- nvim-cmp
local cmp = require('cmp')
plugin_keybinds["cmp"] = {
    ['<c-k>'] = cmp.mapping.select_prev_item(),
    ['<c-j>'] = cmp.mapping.select_next_item(),
    ['<c-n>'] = cmp.mapping.scroll_docs(-4),
    ['<c-p>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<Tab>'] = cmp.mapping.confirm({
        behavior = cmp.ConfirmBehavior.Insert,
        select = true,
    })
}

-- vsnip
-- For some reason this has trouble determining the truthy-ness of vsnip#jumpable.
vim.keymap.set({ "i", "s" }, "<tab>",
    function() return (vim.fn["vsnip#jumpable"](1) ~= 0) and "<Plug>(vsnip-jump-next)" or "<tab>" end, { expr = true })
vim.keymap.set({ "i", "s" }, "<S-tab>",
    function() return (vim.fn["vsnip#jumpable"](-1) ~= 0) and "<Plug>(vsnip-jump-prev)" or "<S-tab>" end, { expr = true })

-- Trouble
local trouble = require("trouble")
vim.keymap.set("n", "<leader>xx", function() trouble.open() end)
vim.keymap.set("n", "<leader>xw", function() trouble.open("workspace_diagnostics") end)
vim.keymap.set("n", "<leader>xd", function() trouble.open("document_diagnostics") end)
vim.keymap.set("n", "<leader>xq", function() trouble.open("quickfix") end)
vim.keymap.set("n", "<leader>xl", function() trouble.open("loclist") end)
vim.keymap.set("n", "gR", function() trouble.open("lsp_references") end)

-- Git-related
plugin_keybinds["gitsigns_callback"] = function(bufnum)
    local gs = package.loaded.gitsigns

    -- Buffer local mappings
    local opts = { buffer = bufnum }
    local expr_opts = { buffer = bufnum, expr = true }
    vim.keymap.set("n", "<leader>gr", gs.reset_hunk, opts)
    vim.keymap.set("n", "<leader>gs", gs.stage_hunk, opts)
    vim.keymap.set("v", "<leader>gr", function() gs.reset_hunk { vim.fn.line("."), vim.fn.line("v") } end)
    vim.keymap.set("v", "<leader>gs", function() gs.stage_hunk { vim.fn.line("."), vim.fn.line("v") } end)
    vim.keymap.set("n", "<leader>gR", gs.reset_buffer, opts)
    vim.keymap.set("n", "<leader>gS", gs.stage_buffer, opts)
    vim.keymap.set("n", "<leader>gu", gs.undo_stage_hunk, opts)
    vim.keymap.set("n", "<leader>gp", gs.preview_hunk, opts)
    vim.keymap.set("n", "<leader>gb", function() gs.blame_line { full = true } end, opts)
    vim.keymap.set("n", "<leader>gd", gs.diffthis, opts)
    vim.keymap.set("n", "<leader>gD", function() gs.diffthis("~") end, opts)

    vim.keymap.set("n", "]c", function()
        if vim.wo.diff then
            return "]c"
        else
            vim.schedule(function() gs.next_hunk() end)
            return "<Ignore>"
        end
    end, expr_opts)

    vim.keymap.set("n", "[c", function()
        if vim.wo.diff then
            return "[c"
        else
            vim.schedule(function() gs.prev_hunk() end)
            return "<Ignore>"
        end
    end, expr_opts)
end


return plugin_keybinds
