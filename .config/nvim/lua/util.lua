local util = {}

-- Appends each element of list b to a.
function util.append_list(a, b)
    for _, v in ipairs(b) do
        table.insert(a, v)
    end

    return a
end

return util

