-- Plugins
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if vim.loop.fs_stat(lazypath) == nil then
    vim.fn.system({
        "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

local util = require('util')
local haslocalopts, local_opts = pcall(require, 'local_opts')

-- TODO move setup to plugins and set properties for actual lazy loading.
local plugins = {
    -- Color schemes
    { "tanvirtin/monokai.nvim" },
    { "sainnhe/gruvbox-material" },

    -- Completion
    "hrsh7th/nvim-cmp",
    "onsails/lspkind.nvim",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "hrsh7th/cmp-nvim-lua",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-calc",

    -- Tools
    "hrsh7th/vim-vsnip",

    -- Syntax
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
    "nvim-treesitter/nvim-treesitter-textobjects",

    -- Git
    -- "f-person/git-blame.nvim",
    "lewis6991/gitsigns.nvim",

    -- LSP
    "neovim/nvim-lspconfig",
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
    {
        "vxpm/ferris.nvim",
        opts = {
            url_handler = "xdg-open",
        }
    },

    -- Other
    { "nvim-telescope/telescope.nvim",   tag = "0.1.3",                                   dependencies = { 'nvim-lua/plenary.nvim' } },
    { "folke/trouble.nvim",              dependencies = { "nvim-tree/nvim-web-devicons" } },
    --    "gorbit99/codewindow.nvim"
    "klen/nvim-config-local",
    "stevearc/dressing.nvim",
}

if haslocalopts then
    util.append_list(plugins, local_opts.plugins or {})
end

require("lazy").setup(plugins)

-- Keybindings. Load here so we can load plugin keybinds.
local plugin_keybinds = require("keybindings")

-- Other includes
require("filetypes")

-- Call any local-only setup.
if haslocalopts and local_opts.setup ~= nil then
    local_opts.setup(plugin_keybinds)
end

require("telescope").setup({
    defaults = {
        mappings = plugin_keybinds["telescope"],
        file_ignore_patterns = {
            "%.o",
            "build/",
            "~$",
        }
    },
})

require("dressing").setup({
    input = {
        trim_prompt = false,
        mappings = plugin_keybinds["dressing"].input
    },
    select = {
        backend = { "telescope", "fzf", "nui", "builtin" },

        telescope = {
            layout_strategy = "cursor",
            layout_config = {
                height = 8,
                width = 50,
            },
            sorting_strategy = 'ascending',
        }
    }
})

require("config-local").setup {
    config_files = { ".nvim.lua", ".nvimrc" },
    hashfile = vim.fn.stdpath("data") .. "/config-local",
    autocommands_create = true, -- VimEnter, DirectoryChanged
    commands_create = true,     -- ConfigLocalSource, ConfigLocalEdit, ConfigLocalTrust, ConfigLocalIgnore
    silent = false,
    lookup_parents = true,
}

--[[
local codewindow = require("codewindow")
require("codewindow").setup({
    auto_enable = true,
    exclude_filetypes = {"help", "man"},
    minimap_width = 15,
    width_multiplier = 5,
    show_cursor = true,
    screen_bounds = "background",
    window_border = "none",
})
codewindow.apply_default_keybinds()
--]]

require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = { "rust_analyzer", "clangd" },
}

local cmp = require('cmp')
local lspkind = require('lspkind')

local cmp_sources = {
    { name = 'path',                    priority = 1 },                     -- file paths
    { name = 'nvim_lsp',                priority = 3 },                     -- from language server
    { name = 'nvim_lsp_signature_help', priority = 3 },
    { name = 'nvim_lua',                priority = 3 },                     -- neovim lua API
    { name = 'buffer',                  keyword_length = 4, priority = 2 }, -- words from current buffer
    { name = 'vsnip',                   priority = 2 },
    { name = 'calc' },                                                      -- Math calculations
}

if haslocalopts then
    util.append_list(cmp_sources, local_opts.cmp_sources or {})
end


cmp.setup({
    snippet = {
        expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
        end,
    },
    preselect = cmp.PreselectMode.Item,
    mapping = plugin_keybinds["cmp"],
    -- General completion settings
    completion = {
        keyword_length = 3,
    },
    -- completion sources
    sources = cmp_sources,
    sorting = {
        priority_weight = 2,
        comparators = {
            cmp.exact,
            cmp.score,
            cmp.locality,
            cmp.recently_used,
            cmp.offset,
            cmp.order,
        }
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    -- TODO configure this to my liking
    formatting = {
        fields = { 'menu', 'abbr', 'kind' },
        format = lspkind.cmp_format({
            mode = "symbol_text",
            menu = {
                nvim_lsp = 'λ',
                nvim_lsp_signature_help = 'λ',
                nvim_lua = '',
                buffer = 'Ω',
                vsnip = '',
                path = '🖫',
                asana = '📝',
                calc = '󰃬',
            },
        }),
    },
})

-- TODO setup mapping for these.
-- cmp.setup.cmdline('/', {
--     mapping = cmp.mapping.preset.cmdline(),
--     completion = {
--         keyword_length = 3,
--     },
--     sources = {
--         { name = 'buffer' },
--     }
-- })
--
-- cmp.setup.cmdline(':', {
--     mapping = cmp.mapping.preset.cmdline(),
--     completion = {
--         keyword_length = 3,
--     },
--     sources = {
--         { name = 'path', option = { max_item_count = 4 } },
--         { name = 'cmdline', option = {ignore_cmds = { 'Man', '!'}, max_item_count = 4} },
--     }
-- })

require("nvim-treesitter.configs").setup {
    ensure_installed = { "c", "lua", "rust", "toml" },
    auto_install = true,
    highlight = {
        enable = true,
        additional_vim_regex_highlighting = false,
    },
    incremental_selection = {
        enable = true,
        keymaps = plugin_keybinds["treesitter"].incremental_selection,
    },
    indent = { enable = false },

    textobjects = {
        select = {
            enable = true,
            lookahead = true,
            keymaps = plugin_keybinds["treesitter"].textobjects.select,
            include_surrounding_whitespace = true,
        },

        move = {
            enable = true,
            set_jumps = true,
            goto_next_start = plugin_keybinds["treesitter"].textobjects.move_goto_next_start,
            goto_next_end = plugin_keybinds["treesitter"].textobjects.move_goto_next_end,
            goto_previous_start = plugin_keybinds["treesitter"].textobjects.move_goto_prev_start,
            goto_previous_end = plugin_keybinds["treesitter"].textobjects.move_goto_prev_end,
        },

        swap = {
            enable = true,
            swap_next = plugin_keybinds["treesitter"].textobjects.swap_next,
            swap_previous = plugin_keybinds["treesitter"].textobjects.swap_previous,
        }
    }
}

require("trouble").setup {
    position = "bottom",
}


-- Setup language servers
local capabilities = require('cmp_nvim_lsp').default_capabilities()
local lspconfig = require('lspconfig')
lspconfig.rust_analyzer.setup {
    capabilities = capabilities,
    settings = {
        ['rust-analyzer'] = {
            imports = {
                granularity = {
                    group = "module",
                    enforce = true,
                },
            },
            cargo = {
                buildScripts = {
                    enable = true,
                },
            },
            check = {
                command = "clippy",
            },
            diagnostics = {
                enable = true,
                experimental = {
                    enable = true,
                },
            },
        }
    },
}
lspconfig.clangd.setup {
    capabilities = capabilities,
}
lspconfig.pyright.setup {
    capabilities = capabilities,
}
lspconfig.tsserver.setup {
    capabilities = capabilities,
}
lspconfig.html.setup {
    capabilities = capabilities,
}
lspconfig.lemminx.setup {
    capabilities = capabilities,
}
lspconfig.cssls.setup {
    capabilities = capabilities,
}
lspconfig.cmake.setup {}
lspconfig.lua_ls.setup {
    on_init = function(client)
        local path = client.workspace_folders[1].name

        -- Skip this config if this has a lua project file.
        if vim.loop.fs_stat(path .. '/.luarc.json') or vim.loop.fs_stat(path .. '/.luarc.jsonc') then
            return
        end

        client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
            -- Select the lua version
            runtime = {
                version = 'LuaJIT'
            },
            -- Add neovim runtime files
            workspace = {
                checkThirdParty = false,
                library = {
                    vim.env.VIMRUNTIME
                }
            }
        })
    end,
    settings = {
        Lua = {}
    }
}
--[[
lspconfig.ruby_lsp.setup {
    capabilities = capabilities,
}
--]]

require("gitsigns").setup {
    on_attach = plugin_keybinds["gitsigns_callback"],
}

-- Options

local os = vim.loop.os_uname()
vim.loader.enable()
vim.opt.guifont = "DejaVuSansM Nerd Font: h12"

-- Set terminal title name
vim.opt.title = true
vim.opt.termguicolors = true
-- vim.opt.titlestring = "nvim %{substitute(getcwd(),'^.*/,'','')}: %t"

vim.opt.compatible = false
vim.opt.number = true
vim.opt.scrolloff = 5     -- keep 5 lines above and below the cursor for context.
vim.opt.sidescrolloff = 2 -- keep 2 columns to the side.
vim.opt.wildignore = "*.o,*~,*.pyc,*.so"
vim.opt.lazyredraw = true -- Don't try to update during macros
vim.opt.showmatch = true
vim.opt.foldenable = false
-- vim.opt.cursorline = true
vim.opt.backspace = "indent,start,eol"
vim.opt.mouse = "a"
vim.opt.updatetime = 500

vim.opt.splitbelow = true
vim.opt.splitright = true

-- Tab config
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.shiftround = true

-- Backup files
--vim.opt.backupdir = "~/.vim-tmp//,~/.tmp//,/var/tmp//,/tmp//"
vim.opt.backupskip = "/tmp/*,/private/tmp/*"
--vim.opt.directory = "~/.vim-tmp//,/var/tmp//,/tmp//"


-- Line wrapping
vim.opt.wrap = false
vim.opt.linebreak = true -- Break lines at specific characters instead of at character limit.

-- Searching
vim.opt.incsearch = true
vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Spell Checking
vim.opt.spelllang = "en_us"

-- Look/Feel
vim.opt.background = "dark"
vim.g.gruvbox_material_background = 'hard'
vim.g.gruvbox_material_better_perforamance = 1
vim.cmd("colorscheme gruvbox-material")

-- Diagnostics
-- TODO set up diagnostic error signs in sign column
vim.diagnostic.config({
    virtual_text = false,    -- Don't show virtual text for diagnostic text
    signs = true,
    update_in_insert = true, -- Update diagnostics when typing in insert mode
    underline = true,
    severity_sort = false,
    float = {
        -- TODO configure these float options.
        source = 'always',
        header = '',
        prefix = '',
    },
})

vim.opt.signcolumn = 'yes'

-- Completion
vim.opt.completeopt = { 'menuone', 'noselect', 'noinsert', 'preview' }
vim.opt.shortmess = vim.opt.shortmess + { c = true }
vim.opt.updatetime = 300

-- treesitter
vim.wo.foldmethod = 'expr'
vim.wo.foldexpr = 'nvim_treesitter#foldexpr()'

-- Autocommands
local agroup = vim.api.nvim_create_augroup("UserLocalCommands", {});

-- Enable spell checking by default in some files types.
vim.api.nvim_create_autocmd("FileType", {
    pattern = "gitcommit,asciidoc,markdown",
    group = agroup,
    callback = function() vim.opt_local.spell = true end
})

-- Enable line wraping for asciidoc files.
vim.api.nvim_create_autocmd("FileType", {
    pattern = "gitcommit,asciidoc,markdown",
    group = agroup,
    callback = function()
        vim.opt_local.wrap = true
        vim.opt_local.textwidth = 100
    end
})

-- Enable some keybinds/functions only when LSP is attached.
vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        local bufnr = ev.buf
        local client_id = ev.data.client_id
        local client = vim.lsp.get_client_by_id(client_id)
        plugin_keybinds["lsp_callback"](bufnr, client)

        if client.server_capabilities.documentFormattingProvider then
            print("Attaching client " .. client_id)
            -- For clangd, disable by default unless the .clang-format file exists.
            if client.name == "clangd" then
                if client.root_dir == nil
                    or vim.fn.findfile(".clang-format", ".;" .. client.root_dir) == "" then
                    vim.b.noformatsave = true
                end
            end
            -- Format on save, unless it's been disabled.
            vim.api.nvim_create_autocmd('BufWritePre', {
                buffer = bufnr,
                callback = function()
                    if not vim.b.noformatsave then
                        vim.lsp.buf.format({
                            async = false,
                            -- Only format if the formatting client is equal to the attaching client.
                            filter = function(c) return c.id == client.id end,
                        })
                    end
                end
            })
        end
    end,
})

--[[
-- Show diagnostics when holding over the line.
vim.api.nvim_create_autocmd('CursorHold', {
    group = agroup,
    callback = function(ev) vim.diagnostic.open_float(nil, { focusable = false }) end
})
--]]

-- User Commands

-- Use :FormatOnSave to toggle or set formatting on save.
vim.api.nvim_create_user_command('FormatOnSave', function(opts)
    local mode = opts.args
    if mode == "off" then
        vim.b.noformatsave = true
    elseif mode == "on" then
        vim.b.noformatsave = false
    else
        vim.b.noformatsave = not vim.b.noformatsave
    end
end, { nargs = '?' })
